﻿namespace AA_tree;

public class AATree
{
    private AANode? _root;

    public AATree()
    {
        _root = null;
    }

    public AANode Skew(AANode node)
    {
        if (node.Left == null)
        {
            return node;
        }

        if (node.Level == node.Left.Level)
        {
            AANode left = node.Left;
            node.Left = left.Right;
            left.Right = node;
            return left;
        }

        return node;
    }
    
    
    public AANode Split(AANode node)
    {
        if (node.Right == null || node.Right.Right == null)
        {
            return node;
        }

        if (node.Level != node.Right.Right.Level) return node;
        
        var right = node.Right;
        node.Right = right.Left;
        right.Left = node;
        right.Level++;
        return right;

    }

    public void Insert(int key)
    {
        _root = Insert(key, _root);
    }

    private AANode Insert(int key, AANode? node)
    {
        if (node == null) return new AANode(key, 1, null, null);
    
        if (key < node.Value) node.Left = Insert(key, node.Left);
    
        else if (key > node.Value) node.Right = Insert(key, node.Right);
    
        node = Skew(node);
        node = Split(node);
        return node;
    }
    

    public bool Find(int value)
    {
        if (_root == null) return false;

        return Find(value, _root);
    }

    private bool Find(int value, AANode node)
    {
        if (node.Value == value) return true;

        if (value < node.Value && node.Left != null) return Find(value, node.Left);

        if (value > node.Value && node.Right != null) return Find(value, node.Right);

        return false;
    }

    public void Remove(int key)
    {
        if (_root == null) return;

        _root = Remove(key, _root);
    }

    private AANode Remove(int key, AANode? node)
    {
        if (node == null) return null!;

        if (key < node.Value) node.Left = Remove(key, node.Left);

        else if (key > node.Value) node.Right = Remove(key, node.Right);

        else if (node.Left != null && node.Right != null)
        {
            var temp = GetMin(node.Right);
            node.Value = temp.Value;
            node.Right = Remove(node.Value, node.Right);
        }

        else node = node.Left ?? node.Right;

        if (node == null) return node!;

        node = Skew(node);
        node = Split(node);

        return node;
    }

    private AANode GetMin(AANode node) => node.Left == null ? node : GetMin(node.Left);

    public void Print() => Print(_root);

    private void Print(AANode? node)
    {
        if (node == null) return;
        Print(node.Left);
        Console.Write(node.Value + " ");
        Print(node.Right);
    }

    public AANode? GetRoot() => _root;

    public bool IsEmpty() => _root == null;

    public void Clear() => _root = null;
}