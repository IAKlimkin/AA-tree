﻿using System;
using System.Threading.Channels;

namespace AA_tree;

public class Program
{
    internal static void Main(string[] args)
    {
        var tree = new AATree();
        tree.Insert(5);
        tree.Insert(3);
        tree.Insert(2);
        tree.Insert(4);
        tree.Insert(6);
        tree.Insert(1);


        Console.WriteLine(" ");
        Console.WriteLine(tree.GetRoot().Value);
        // tree.Remove(3);

        tree.Print();
        
        //Console.WriteLine(tree.Find(3));
    }
}