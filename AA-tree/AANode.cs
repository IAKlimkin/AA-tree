﻿namespace AA_tree;

public class AANode
{
    public int Value;
    public AANode? Left;
    public AANode? Right;
    public int Level;

    public AANode(int value, int level, AANode? left, AANode? right)
    {
        Value = value;
        Level = level;
        Left = left;
        Right = right;
    }
}